-- First you need to create a schema to hold your data for this chapter.
CREATE SCHEMA ch01;

-- Next you need to create a lookup table to map franchise codes to meaningful names.
CREATE TABLE ch01.lu_franchises (
  id char(3) PRIMARY KEY,
  franchise varchar(30)
);

-- You can then add all the franchises you'll be dealing with.
INSERT INTO ch01.lu_franchises(id, franchise)
VALUES
  ('BKG', 'Burger King'),
  ('CJR', 'Carl''s Jr'),
  ('HDE', 'Hardee'),
  ('INO', 'In-N-Out'),
  ('JIB', 'Jack in the Box'),
  ('KFC', 'Kentucky Fried Chicken'),
  ('MCD', 'McDonald'),
  ('PZH', 'Pizza Hut'),
  ('TCB', 'Taco Bell'),
  ('WDY', 'Wendy''s');

-- Finally, you need to create a table to hold the data you'll be holding.
CREATE TABLE ch01.restaurants (
  id serial PRIMARY KEY,
  franchise char(3),
  geom geometry(point,2163)
);

-- Next you need to place a spatial index on your geometry column.
CREATE INDEX idx_code_restaurants_geom ON ch01.restaurants USING gist(geom);

-- Create a foreign key relationship between the franchise column in the restaurants table and the lookup table.
ALTER TABLE ch01.restaurants
ADD CONSTRAINT fk_restaurants_lu_franchises
FOREIGN KEY (franchise)
REFERENCES ch01.lu_franchises (id)
ON UPDATE CASCADE
ON DELETE RESTRICT;

-- Next you need to create a highways table to contain the road segments that are highways.
CREATE TABLE ch01.highways (
  gid integer NOT NULL,
  feature character varying(80),
  name character varying(120),
  state character varying(2),
  geom geometry(multilinestring,2163),
  CONSTRAINT pk_highways PRIMARY KEY (gid)
);


-- Fastfoodmaps.com graciously provided us with a comma-delimited file of all fast food restaurants circa 2005.
CREATE TABLE ch01.restaurants_staging (
  franchise text,
  lat double precision,
  lon double precision
);

-- Use the copy command to import the CSV file into your staging file.
COPY ch01.restaurants_staging
FROM '/home/user/Desktop/postgis_in_action/ch01/data/restaurants.csv'
DELIMITER AS ',';

-- The data passes the quality check, so you can proceed with the insert.
INSERT INTO ch01.restaurants (franchise, geom)
SELECT
  franchise,
  ST_Transform(ST_SetSRID(ST_Point(lon, lat), 4326), 2163) AS geom
FROM ch01.restaurants_staging;

-- See highway_data_quality_investigation.sql for some deeper inspection.
SELECT *
FROM ch01.highways_staging;

-- After you're satisfied  that the importer did its job without dropping
-- any information, move the data from your staging table to the production table.
-- Also filter for major highways.
INSERT INTO ch01.highways (gid, feature, name, state, geom)
SELECT gid, feature, name, state, ST_Transform(geom, 2163)
FROM ch01.highways_staging
WHERE feature LIKE 'Principal Highway%';

-- After you've finished loading the data, it's a good idea to follow up
-- with a vacuum analyze so that the statistics are up to date.
VACUUM ANALYZE ch01.highways;

-- How many fast food restaurants are within one mile of a highway?
-- The unit distance of spatial reference system 2163 is one meter.
-- There are about 1609 meters in a mile.
SELECT f.franchise, COUNT(DISTINCT r.id) AS total
FROM       ch01.restaurants AS r
INNER JOIN ch01.highways    AS h ON ST_DWithin(r.geom, h.geom, 1609)
INNER JOIN ch01.lu_franchises AS f ON r.franchise = f.id
GROUP BY f.franchise
ORDER BY total DESC;
